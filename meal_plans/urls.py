from django.urls import path

from meal_plans.views import (
    MealPlansListView,
    MealPlansCreateView,
    MealPlansDetailView,
    MealPlansUpdateView,
    MealPlanDeleteView,
)

urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plan_list"),
    path(
        "<int:pk>/",
        MealPlansDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "create/",
        MealPlansCreateView.as_view(),
        name="meal_plan_new",
    ),
    path(
        "<int:pk>/edit/",
        MealPlansUpdateView.as_view(),
        name="meal_plan_edit",
    ),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
