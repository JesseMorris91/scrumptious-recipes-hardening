from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plans.models import MealPlans


class MealPlansListView(LoginRequiredMixin, ListView):
    model = MealPlans
    template_name = "mealplans/list.html"
    context_object_name = "mealsplans_list"

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlans
    template_name = "mealplans/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plan_detail", pk=plan.id)

    def get_success_url(self):
        return reverse_lazy("meal_plan_list")


class MealPlansDetailView(LoginRequiredMixin, DetailView):
    name = MealPlans
    template_name = "mealplans/detail.html"
    fields = ["name", "recipes", "date"]
    context_object_name = "mealplansdetail"

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlans
    template_name = "mealplans/edit.html"
    fields = [
        "name",
        "owner",
        "description",
    ]

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan_detail", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlans
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("meal_plan_list")

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)
