# Generated by Django 4.0.3 on 2022-09-05 22:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0009_shoppingitem'),
        ('meal_plans', '0002_alter_mealplans_recipes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mealplans',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ownermeal', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='mealplans',
            name='recipes',
            field=models.ManyToManyField(related_name='meal_plans', to='recipes.recipe'),
        ),
    ]
