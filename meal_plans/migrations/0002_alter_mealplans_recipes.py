# Generated by Django 4.0.3 on 2022-09-01 20:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0009_shoppingitem'),
        ('meal_plans', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mealplans',
            name='recipes',
            field=models.ManyToManyField(related_name='recipe', to='recipes.recipe'),
        ),
    ]
