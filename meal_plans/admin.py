from django.contrib import admin

from meal_plans.models import MealPlans

admin.site.register(MealPlans)
