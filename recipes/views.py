from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods
from recipes.forms import RatingForm
from django.db import IntegrityError

from recipes.models import Recipe


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")

            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


@require_http_methods(["POST"])
def CreateShoppingItem(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)


# Get the value for the "ingredient_id" from the
# request.POST dictionary using the "get" method

# Get the specific ingredient from the Ingredient model
# using the code
# Ingredient.objects.get(id=the value from the dictionary)
# user = request.user
# Get the current user which is stored in request.user
# try:
# ShoppingItem.objects.create(food_item=ingredient., user=request.user)
# try:
# Create the new shopping item in the database
# using ShoppingItem.objects.create(
#   food_item= the food item on the ingredient,
#   user= the current user
# )
# except IntegrityError:  # Raised if someone tries to add
# pass                # the same food twice, just ignore it
#   except IntegrityError
# Go back to the recipe page with a redirect
# to the name of the registered recipe detail
# path with code like this
# return redirect(
#     name of the registered recipe detail path,
#     pk=id of the ingredient's recipe
# )
